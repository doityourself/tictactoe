- add additional information to perror (e.g how much info were we trying to alloc)
- format patch check
- add player's implementation
- check for no trailing whitespaces before merge, ideally during CI pipeline

# misc:
- would be good to have tool to check that all file that were opened are closed
