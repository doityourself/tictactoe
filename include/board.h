#ifndef __BOARD_H__
#define __BOARD_H__

#include "common.h"

struct board {
	uint32_t size;
	uint32_t **cell;
};

struct player {
	int a;
};

/**
 * init_board - initialize the game board
 *
 * @param board	 board to be created
 * @param size	the created board will be size by size
 *
 * @return ptr to a struct board instance if successful, 
 * error code otherwise
 */
struct board * init_board(int size);

/**
 * make_move - update the game cell with the requested move
 *
 *
 * @param board	 board to be updated 
 * @param player structer containing player ID and requested move
 *
 * @return 0 if successful, error code otherwise
 */
int make_move(struct board *board, struct player player);

/**
 * check_win - check if there is win
 *
 *
 * @param board	 board to be checked 
 * @param player structer containing player ID and last move
 *
 * @return 1 if player has won, 0 otherwise
 */
bool check_win(struct board *board, struct player player);

/**
 * delete_board - delete the game board
 *
 * @return void
 */
void delete_board(struct board *board);

#endif /* __BOARD_H_ */
