#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include "game_param.h"

#ifdef GTEST
#include "gtest_common.hpp"
#endif

// Error management
#define ERROR -1
#define SUCCESS 0
#define MAX_ERRNO 4095

#define IS_ERR_VALUE(x) ((unsigned long)(void *)(x) >= (unsigned long)-MAX_ERRNO)

static inline unsigned long IS_PTR_ERR(void *ptr)
{
	return IS_ERR_VALUE((unsigned long)ptr);
}

static inline void * ERR_PTR(long error)
{
	return (void *) error;
}

#ifndef DEBUG
#undef pr_debug
#define pr_debug(...)
#endif

#define PRE_LOG(fd, log_id) \
	fprintf(fd, "%s: (%s:%s:%d): ", #log_id, \
			__FILE__, __func__, __LINE__);

#define LOG(fd, log_id, ...) \
	PRE_LOG(fd, log_id); \
	fprintf(fd, ##__VA_ARGS__);

#ifndef pr_info
#define pr_info(...) LOG(stdout, INFO, ##__VA_ARGS__)
#endif

#ifndef pr_debug
#define pr_debug(...) LOG(stdout, DEBUG, ##__VA_ARGS__)
#endif

#ifndef pr_error
#define pr_error(...) LOG(stderr, ERROR, ##__VA_ARGS__)
#endif

#ifndef perror
#define perror(err_msg) \
	PRE_LOG(stderr, PERROR); \
	perror(err_msg)
#endif

#endif /* __COMMON_H__ */
