#ifndef __DISP_H__
#define __DISP_H__

#include <board.h>

extern const char player_syms[];

/**
 * disp_board - display board on stdout
 *
 * @param board board to be display
 *
 * @return void
 */
void disp_board(struct board board);

#endif /* __DISP_H__ */
