#include <board.h>

struct board * init_board(int size)
{
	struct board *board;
	uint32_t **cell;
	int ret, i;

	// check requested size
	if (size < BOARD_SIZE_MIN) {
		ret = -EINVAL;
		pr_error("size must be >= %u\n", BOARD_SIZE_MIN);
		goto ret_err;
	}

	// allocate board structure
	board = (struct board *) calloc(1, sizeof(struct board));
	if (!board) {
		ret = -ENOMEM;
		perror("");
		goto ret_err;
	}
	
	// allocate board cell
	cell = (uint32_t **) malloc(size * sizeof(uint32_t *));
	if (!cell) {
		ret = -ENOMEM;
		perror("");
		goto deallocate_board;
	}

	for (i = 0; i<size; i++) {
		cell[i] = (uint32_t *) calloc(size, sizeof(uint32_t));
		if (!cell[i]) {
			ret = -ENOMEM;
			perror("");
			goto deallocate_cell;
		}
	}
	
	// initialize board size
	board->size = (uint32_t) size;
	
	// initialize board cell
	board->cell = cell;

	return board;

deallocate_cell:
	while(i--) {
		free(cell[i]);
	}
	free(cell);

deallocate_board:
	free(board);

ret_err:
	return ERR_PTR(ret);

}

void delete_board(struct board *board)
{
	// free board cell
	while(board->size--)
		free(board->cell[board->size]);
	free(board->cell);
	
	// free board structure
	free(board);
}
