#include "disp.h"

const char player_syms[] = {'-', 'x', 'o'};

void disp_board(struct board board)
{
	uint32_t i, j;
	char player_sym;

	for(i = 0; i<board.size; i++) {
		printf("|");
		for(j = 0; j<board.size; j++) {
			player_sym = player_syms[board.cell[i][j]];
			printf("%c|", player_sym);
		}
		printf("\n");
	}	
}
