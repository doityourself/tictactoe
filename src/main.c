#include "game_param.h"
#include "board.h"
#include "disp.h"

int main(void)
{
	struct board *board;
	
	// init game board of size BOARD_SIZE
	board = init_board(BOARD_SIZE);
	if (IS_PTR_ERR(board)) {
		pr_error("Exit on failure !\n");
		exit(1);
	}

	// clean dynamically allocated mem
	delete_board(board);

	return 0;
}
