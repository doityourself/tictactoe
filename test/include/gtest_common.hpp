#ifndef __GTEST_COMMON_HPP__
#define __GTEST_COMMON_HPP__

#ifdef GTEST_DEBUG
#undef DEBUG
#define DEBUG
#else
extern FILE *stdnull;

#undef printf
#define printf(...) fprintf(stdnull, __VA_ARGS__)

#undef pr_error
#define pr_error(...) fprintf(stdnull, __VA_ARGS__)

#undef pr_debug
#define pr_debug(...) fprintf(stdnull, __VA_ARGS__)

#undef pr_debug
#define pr_debug(...) fprintf(stdnull, __VA_ARGS__)

#undef pr_info
#define pr_info(...) fprintf(stdnull, __VA_ARGS__)
#endif /* GTEST_DEBUG */

#endif /* __GTEST_COMMON_HPP__ */
