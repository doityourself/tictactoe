#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1

TEST_SUFFIX="_tests"

for TEST in *${TEST_SUFFIX}
do
	./${TEST}
done
