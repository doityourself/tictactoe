#include "gtest/gtest.h"


extern "C" {
#include "board.h"
}

TEST(BoardTest, HandlesSizeInput) {
	struct board *board;
	int i, steps, size;

	EXPECT_GT(BOARD_SIZE_MIN, 0);
	
	// Good input sizes
	steps = 10;
	for(i = 1; i<steps; i++) {
		size = BOARD_SIZE_MIN * i;
		board = init_board(size);
		ASSERT_EQ(size, board->size);
		ASSERT_FALSE(IS_PTR_ERR(board));
		delete_board(board);
	}

	// Bad input sizes
	for(i = -1; i<BOARD_SIZE_MIN; i++) {
		board = init_board(i);
		ASSERT_TRUE(IS_PTR_ERR(board));
	}
}

TEST(BoardTest, ZeroedCells) {
	struct board *board;

	board = init_board(BOARD_SIZE_MIN);
	ASSERT_FALSE(IS_PTR_ERR(board));

	for(int i = 0; i<BOARD_SIZE_MIN; i++) {	
		for(int j = 0; j<BOARD_SIZE_MIN; j++) {	
			EXPECT_EQ(0, board->cell[i][j]);
		}
	}
}
