#include "gtest/gtest.h"

#include "gtest_common.hpp"
	
FILE *stdnull = fopen("/dev/null", "w");

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS() && fclose(stdnull);
}
